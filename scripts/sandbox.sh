#!/bin/bash

sh ./scripts/shared.sh
cp -rf ./shared/ ./sandbox/shared/

server=./node_modules/.bin/webpack-dev-server
$server --inline --config ./webpack.sandbox.js --content-base sandbox/
