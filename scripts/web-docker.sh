#!/bin/bash

sh ./scripts/shared.sh
cp -rf shared/ web/shared/

server=./node_modules/.bin/webpack-dev-server
$server --host=0.0.0.0 --inline --config ./webpack.web.js --content-base web/